#include <ctype.h>


#define CORRECT_PASSWORD "9510"
#define CORRECT_NIGHT_PASSWORD "0159"


#define bit9600Delay 100  
#define halfBit9600Delay 50
#define bit4800Delay 188
#define halfBit4800Delay 94


//LCD
#include "Wire.h"
#include "LiquidCrystal_AIP31068_I2C.h"
LiquidCrystal_AIP31068_I2C  LCD(0x3E,16,2);

//LIGHT
#define PIN_LED 45

//LEDS
#define RED1 30 
#define RED2 33 
#define RED3 36 
#define RED4 39 
#define RED5 42 

#define GRN1 31 
#define GRN2 34 
#define GRN3 37 
#define GRN4 40 
#define GRN5 43 

#define BLU1 32
#define BLU2 35
#define BLU3 38 
#define BLU4 41
#define BLU5 44
int nled;

#define BUZZER_PIN 11

//KEYPAD
#include <Keypad.h>
const byte ROWS = 4;
const byte COLS = 3;

char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};
byte rowPins[ROWS] = {3, 4, 5, 6};
byte colPins[COLS] = {9,8,7};

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

char keypress;
int numberOfErrors;

//CLOCK
#include <Wire.h>
#include "RTClib.h"

RTC_DS1307 rtc;
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

//VAR
int time;
int state;
// 1 => IDLE
// 2 => WAITING PASSWORD
// 3 => VALIDATING PASSWORD
// 4 => ERROR PASSWORD
// 5 => WAITING NIGHT PASSWORD
// 6 => VALIDATING NIGHT PASSWORD
// 7 => ERROR NIGHT PASSWORD
// 8 => BLOCKED
String currentPasswordEntered = "";
//---------------------------------------------------------------

void setup()
{
	time=0;
	Serial.begin(9600);
	state = 1; // set to IDLE
	currentPasswordEntered = "";
	int numberOfErrors = 0;

	
	//LEDS
	pinMode(RED1, OUTPUT);pinMode(GRN1, OUTPUT);pinMode(BLU1, OUTPUT);
	pinMode(RED2, OUTPUT);pinMode(GRN2, OUTPUT);pinMode(BLU2, OUTPUT);
	pinMode(RED3, OUTPUT);pinMode(GRN3, OUTPUT);pinMode(BLU3, OUTPUT);
	pinMode(RED4, OUTPUT);pinMode(GRN4, OUTPUT);pinMode(BLU4, OUTPUT);
	pinMode(RED5, OUTPUT);pinMode(GRN5, OUTPUT);pinMode(BLU5, OUTPUT);
	int nled = 0;
	
	//BUZZER
	buzzerStart();
	
	//CLOCK
	if (!rtc.begin()) {
    		Serial.println("Couldn't find RTC");
    		while (1);
  	}
	if (!rtc.isrunning()) {
    		Serial.println("RTC stopped");
    		rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
 	}

}	
//---------------------------------------------------------------

//LEDS
void greenLeds(){
	ledsOff();
	digitalWrite(RED1, LOW);digitalWrite(GRN1, HIGH);digitalWrite(BLU1, LOW);
	digitalWrite(RED2, LOW);digitalWrite(GRN2, HIGH);digitalWrite(BLU2, LOW);
	digitalWrite(RED3, LOW);digitalWrite(GRN3, HIGH);digitalWrite(BLU3, LOW);
	digitalWrite(RED4, LOW);digitalWrite(GRN4, HIGH);digitalWrite(BLU4, LOW);
	digitalWrite(RED5, LOW);digitalWrite(GRN5, HIGH);digitalWrite(BLU5, LOW);
}

void redLeds(){
	ledsOff();
	digitalWrite(RED1, HIGH);digitalWrite(GRN1, LOW);digitalWrite(BLU1, LOW);
	digitalWrite(RED2, HIGH);digitalWrite(GRN2, LOW);digitalWrite(BLU2, LOW);
	digitalWrite(RED3, HIGH);digitalWrite(GRN3, LOW);digitalWrite(BLU3, LOW);
	digitalWrite(RED4, HIGH);digitalWrite(GRN4, LOW);digitalWrite(BLU4, LOW);
	digitalWrite(RED5, HIGH);digitalWrite(GRN5, LOW);digitalWrite(BLU5, LOW);
}

void blinkRedLeds() {
	ledsOff();
	
	redLeds();
	delay(750);
	
	ledsOff();
	delay(750);

	redLeds();
	delay(750);
			
	ledsOff();
	delay(750);

	redLeds();
	delay(750);
	
	ledsOff();
}

void orangeLeds(){
	ledsOff();
	if(nled%5 == 0){
		digitalWrite(RED1, HIGH);digitalWrite(GRN1, HIGH);digitalWrite(BLU1, LOW);
	}
	if(nled%5 == 1){
		digitalWrite(RED2, HIGH);digitalWrite(GRN2, HIGH);digitalWrite(BLU2, LOW);
	}
	if(nled%5 == 2){
		digitalWrite(RED3, HIGH);digitalWrite(GRN3, HIGH);digitalWrite(BLU3, LOW);
	}
	if(nled%5 == 3){
		digitalWrite(RED4, HIGH);digitalWrite(GRN4, HIGH);digitalWrite(BLU4, LOW);
	}
	if(nled%5 == 4){
		digitalWrite(RED5, HIGH);digitalWrite(GRN5, HIGH);digitalWrite(BLU5, LOW);
	}
	nled = nled + 1;
}

void ledsOff(){
	digitalWrite(RED1, LOW);digitalWrite(GRN1, LOW);digitalWrite(BLU1, LOW);
	digitalWrite(RED2, LOW);digitalWrite(GRN2, LOW);digitalWrite(BLU2, LOW);
	digitalWrite(RED3, LOW);digitalWrite(GRN3, LOW);digitalWrite(BLU3, LOW);
	digitalWrite(RED4, LOW);digitalWrite(GRN4, LOW);digitalWrite(BLU4, LOW);
	digitalWrite(RED5, LOW);digitalWrite(GRN5, LOW);digitalWrite(BLU5, LOW);
}

//LCD
void lcdOff(){
	 LCD.noDisplay();
}

void lcdShutdown(){
	LCD.init();
	LCD.setCursor(2, 5);
  	LCD.print("GOODBYE");
   	LCD.display();
}

void lcdAskPassword(){
	LCD.init();
	LCD.setCursor(2, 5);
  	LCD.print("ENTER PASSWORD");
   	LCD.display();
}

void lcdAskNightPassword(){
	LCD.init();
	LCD.setCursor(2, 5);
  	LCD.print("ENTER NIGHT PASSWORD");
   	LCD.display();
}

void lcdCorrectPassword(){
	LCD.init();
	LCD.setCursor(2, 5);
  	LCD.print("CORRECT");
   	LCD.display();
}

void lcdWrongPassword(){
	LCD.init();
	LCD.setCursor(2, 5);
  	LCD.print("WRONG");
   	LCD.display();
}

void lcdBlocked(){
	LCD.init();
	LCD.setCursor(2, 5);
  	LCD.print("BLOCKED");
   	LCD.display();

}

//BUZZER
void buzzerStop(){
	noTone(BUZZER_PIN);
}

void buzzerShortBip(){
	tone(BUZZER_PIN, 200);
	delay(200);
	buzzerStop();
}

void buzzerStart(){
	tone(BUZZER_PIN, 300);
	delay(1000);
	
	buzzerStop();
	delay(1000);
	
	tone(BUZZER_PIN, 300);
	delay(1000);
	
	buzzerStop();
	delay(1000);
	
	tone(BUZZER_PIN, 300);
	delay(1000);
	
	buzzerStop();
	delay(1000);
}

void buzzerOk(){
	tone (BUZZER_PIN, 200);
	delay(1000);
	buzzerStop();
}

void buzzerError(){
	tone (BUZZER_PIN, 400);
	delay(1000);
	buzzerStop();
}

// CLOCK
boolean isNight(){
	DateTime now = rtc.now();
	if(now.hour() < 6 && 20 > now.hour()) {
		return false;
	}
	return true;
}

//STATE
void stateIdle(){
	greenLeds();
	while(keypress == NO_KEY){
		delay(100);
		time += 100;
		keypress = keypad.getKey();
		
	}
	
	if(isNight()){
		digitalWrite(PIN_LED, HIGH);
	}else{
		digitalWrite(PIN_LED, LOW);
	}
	
	if (keypress){
		state = 2;
		keypress = NO_KEY;
		currentPasswordEntered = "";
		buzzerShortBip();
	}

}

void stateWaitingPassword(){
	lcdAskPassword();

	while(keypress == NO_KEY){
		keypress = keypad.getKey();
		if(time%500 == 0) orangeLeds();
		delay(100);
		time+=100;
	}
	if(keypress == '*'){
		state = 3;
		
	}else if(keypress == '#'){
		state = 1;
		lcdShutdown();
	}else {
		currentPasswordEntered += keypress;
		buzzerShortBip();
		Serial.println(currentPasswordEntered);
	}
	if (keypress){
		keypress = NO_KEY;
	}
}

void stateValidatingPassword(){
	if(currentPasswordEntered.equals(CORRECT_PASSWORD)){
		buzzerOk();
		lcdCorrectPassword();
		if(!isNight()){
			state = 1;
		}else{
			state = 5;
		}
		numberOfErrors = 0;
		currentPasswordEntered= "";
	}else{
		state = 4;
	}
}

void stateErrorPassword(){
	buzzerError();
	redLeds();
	lcdWrongPassword();
	
	blinkRedLeds();
	
	numberOfErrors = numberOfErrors + 1;
	if (numberOfErrors <= 3) {
		state = 1;
	} else {
		state = 8;
	}
}

void stateWaitingNightPassword(){
	lcdAskNightPassword();

	while(keypress == NO_KEY){
		keypress = keypad.getKey();
		if(time%500 == 0) orangeLeds();
		delay(100);
		time+=100;
	}
	if(keypress == '*'){
		state = 6;
		
	} else if(keypress == '#'){
		state = 1;
		lcdShutdown();
	} else {
		currentPasswordEntered += keypress;
		buzzerShortBip();
		Serial.println(currentPasswordEntered);
	}
	if (keypress){
		keypress = NO_KEY;
	}
}

void stateValidatingNightPassword(){
	if(currentPasswordEntered.equals(CORRECT_NIGHT_PASSWORD)){
		buzzerOk();
		lcdCorrectPassword();
		state = 1;
		numberOfErrors = 0;
		currentPasswordEntered= "";
	} else {
		state = 7;
	}
}

void stateErrorNightPassword(){
	buzzerError();
	redLeds();
	lcdWrongPassword();
	
	blinkRedLeds();
	
	numberOfErrors = numberOfErrors + 1;
	if (numberOfErrors <= 3) {
		state = 1;
	} else {
		state = 8;
	}
}

void stateBlocked(){
	redLeds();
	lcdBlocked();
}

void loop() {
	//IDLE
	if (state == 1){
		Serial.println("IDLE");
		stateIdle();
	}

	//WAITING FIRST PASSWORD
	if(state == 2){
		Serial.println("WAITING FIRST PASSWORD");
		stateWaitingPassword();
	}
		
	//VALIDATING PASSWORD
	if(state == 3){
		Serial.println("VALIDATING PASSWORD");
		stateValidatingPassword();
	}

	//ERROR PASSWORD
	if(state == 4){
		Serial.println("ERROR PASSWORD");
		stateErrorPassword();
	}
	
	//WAITING NIGHT PASSWORD
	if(state == 5){
		Serial.println("WAITING NIGHT PASSWORD");
		stateWaitingNightPassword();
	}
	
	//VALIDATING NIGHT
	if(state == 6){	
		Serial.println("VALIDATING NIGHT");
		stateValidatingNightPassword();
	}

	//ERROR NIGHT PASSWORD
	if(state == 7){
		Serial.println("ERROR NIGHT PASSWORD");
		stateErrorNightPassword();
	}
	
	// BLOCKED
	if(state == 8){
		Serial.println("BLOCKED");
		stateBlocked();
	}
};
